import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router) {}

  canActivate(): boolean {
    const auth = localStorage.getItem('token');

    if (auth) {
      return true; // User is authenticated, allow access to the route
    } else {
      this.router.navigate(['/signin']); // Redirect to the login page
      return false; // Block access to the route
    }
  }
}
