import { catchError } from 'rxjs/operators';
import { Component, Input, ViewChild } from '@angular/core';
import { FormsModule, NgForm } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AuthService } from '../services/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { LoadingComponent } from '../common/components/loading/loading.component';
import { Router, RouterLink, RouterModule } from '@angular/router';

import { object, string, number, date, InferType } from 'yup';

const schema = object({
  firstName: string().required(),
  lastName: string().required(),
  phoneNumber: string().required(),
  email: string().email().required(),
  password: string().required(),
  confirmPassword: string().required(),
});

@Component({
  selector: 'app-signup',
  standalone: true,
  imports: [
    CommonModule,
    // * - <Form /> -> ng Form Directive -> ngForm {}
    // * - Doesn't detect input fields automatically
    FormsModule,
    RouterLink,
    HttpClientModule,
    LoadingComponent,
  ],
  templateUrl: './signup.component.html',
  providers: [AuthService, RouterModule],
})
export class SignupComponent {
  // @ViewChild('f', { static: false }) signupForm: NgForm;

  // Get Post ID from URL
  private User: any;

  // Handling Fetching Post Loading State
  public isLoading: boolean = false;
  public serverError: string = '';
  private userId: string = '';

  constructor(private AuthService: AuthService, private Router: Router) {
    // Initialize The ngForm
    this.User = {
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      confirmPassword: '',
    };

    // Initialize The Page as Loading
    this.isLoading = true;
  }

  onSubmit(form: NgForm) {
    console.log(form);
    this.AuthService.signUp(form.value).subscribe({
      next: (res: any) => {
        this.userId = res.data.routeId;
      },
      error: (err: any) => {
        this.serverError = err.error.message;
      },
      complete: () => {
        this.Router.navigate([`/signin`]); // Redirect to the login page
      },
    });
  }

  ngOnInit() {}
}

// this.postId = <string>this.route.snapshot.paramMap.get('id');

// return this.postService.getOne(Number(this.postId)).subscribe((post) => {
//   this.post = post;

//   this.userService.getOne(this.post.userId).subscribe((user) => {
//     this.user = user;
//     this.isLoading = false;
//   });
// });
