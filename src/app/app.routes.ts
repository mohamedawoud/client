import { Routes } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { SignupComponent } from './signup/singup.component';
import { SigninComponent } from './signin/singin.component';
import { AuthGuard } from '../guards/auth.guard';
import { UserComponent } from './user/user.component';

export const routes: Routes = [
  // Authantication
  {
    path: 'signin',
    title: 'Sign in',
    component: SigninComponent,
  },

  {
    path: 'signup',
    title: 'Sign up',
    component: SignupComponent,
    canActivate: [AuthGuard],
  },

  // Resources
  {
    path: 'user/:id',
    title: 'User',
    component: UserComponent,
    canActivate: [AuthGuard],
  },

  // Fallback 404
  { path: '**', component: PageNotFoundComponent },
];
