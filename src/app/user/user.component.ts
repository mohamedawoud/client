import { AuthService } from './../services/auth.service';
import { UserService } from './../services/user.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, NgForm } from '@angular/forms';

@Component({
  selector: 'app-user',
  standalone: true,
  imports: [CommonModule, HttpClientModule, FormsModule],
  templateUrl: './user.component.html',
  providers: [UserService, RouterModule],
})
export class UserComponent implements OnInit {
  @ViewChild('f') form!: NgForm;

  // Fields
  private userId: string = '';
  public user: any = {};

  constructor(
    // Injections
    private UserService: UserService,
    private route: ActivatedRoute
  ) {}

  onSubmit(f: NgForm) {
    this.UserService.update(this.userId, f.value).subscribe((res: any) => {
      console.log('🚀 ~ UserComponent ~ onSubmit ~ res', res);
    });
  }

  ngOnInit() {
    this.userId = <string>this.route.snapshot.paramMap.get('id');

    this.UserService.getOne(this.userId).subscribe((res: any) => {
      this.user = res.data;
    });
  }
}
