import { date, object } from 'yup';
import { Router } from '@angular/router';
import { AuthService } from './../../services/auth.service';
import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, NgForm } from '@angular/forms';
import { ISucessfulSignin } from '../signin-form/signin-form.component';

interface otpResponse {
  token: string;
}

@Component({
  selector: 'app-otp',
  standalone: true,
  imports: [CommonModule, FormsModule],
  providers: [AuthService],
  templateUrl: './otp.component.html',
})
export class OtpComponent {
  @Input() public data: ISucessfulSignin | undefined;

  constructor(private AuthService: AuthService, private Router: Router) {}

  onSubmit(form: NgForm) {
    const otp = Object.values(form.value).join('');

    return this.AuthService.verifyOtp({
      otp,
      email: this.data?.email,
      remember: this.data?.remember,

      // TODO - fix type
    }).subscribe((res: any) => {
      console.log('🚀 ~ OtpComponent ~ onSubmit ~ res:', res);

      localStorage.setItem('token', res.data.token);
      this.Router.navigate([`/user/${res.data.routeId}`]); // Redirect to the login page
    });
  }
}
