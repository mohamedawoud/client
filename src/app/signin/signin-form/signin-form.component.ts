import { Post } from '../../model/post.model';
import { Component, EventEmitter, Output } from '@angular/core';
import { FormsModule, NgForm } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AuthService } from '../../services/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { LoadingComponent } from '../../common/components/loading/loading.component';
import { Router, RouterLink, RouterModule } from '@angular/router';
import { OtpComponent } from '../otp/otp.component';

export interface ISucessfulSignin {
  email: string;
  remember: boolean;
}

@Component({
  selector: 'app-signin-form',
  standalone: true,
  imports: [
    // Modules
    CommonModule,
    // * - <Form /> -> ng Form Directive -> ngForm {}
    // * - Doesn't detect input fields automatically
    FormsModule,
    RouterLink,
    HttpClientModule,

    // Components
    OtpComponent,
    LoadingComponent,
  ],
  templateUrl: './signin-form.component.html',
  providers: [AuthService, RouterModule],
})
export class SigninFormComponent {
  @Output() onSuccessfulSignIn = new EventEmitter<ISucessfulSignin>();

  constructor(private AuthService: AuthService, private router: Router) {}

  onSubmit(form: NgForm) {
    const { rememberMe, email, password } = form.value;

    return this.AuthService.amIAuthorized({ email, password }).subscribe(
      (res: any) => {
        console.log('🚀 ~ SigninFormComponent ~ onSubmit ~ res:', res);
        return (
          res &&
          this.onSuccessfulSignIn.emit({
            email,
            remember: rememberMe ? true : false,
          })
        );
      }
    );
  }

  ngOnInit() {}
}
