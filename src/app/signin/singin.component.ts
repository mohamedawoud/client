import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingComponent } from '../common/components/loading/loading.component';
import { OtpComponent } from './otp/otp.component';
import {
  ISucessfulSignin,
  SigninFormComponent,
} from './signin-form/signin-form.component';

@Component({
  selector: 'app-signup',
  standalone: true,
  imports: [
    // Modules
    CommonModule,

    // Components
    SigninFormComponent,
    OtpComponent,
    LoadingComponent,
  ],
  templateUrl: './signin.component.html',
  providers: [],
})
// Implementaion
export class SigninComponent {
  public successfulSignIn: ISucessfulSignin | undefined;
  public isRememberMe: boolean = false;

  constructor() {}

  handleSuccessfulSignIn(data: ISucessfulSignin) {
    this.successfulSignIn = data;
  }

  ngOnInit() {}
}
