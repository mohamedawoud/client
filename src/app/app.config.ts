import { ApplicationConfig } from '@angular/core';
import { provideRouter } from '@angular/router';

import { routes } from './app.routes';
import { UserService } from './services/user.service';
import { AuthService } from './services/auth.service';
import { DataService } from './services/data.service';

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes),
    DataService,
    AuthService,
    UserService,

    // { provide: 'API_URL', useValue: 'https://jsonplaceholder.typicode.com' },
  ],
};
