import { date } from 'yup';
import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

interface Isignin {
  email: string;
  password: string;
}

interface Isingup {}

interface User {}

@Injectable({ providedIn: 'root' })
export class AuthService {
  private url: string;

  constructor(private http: HttpClient) {
    this.url = `http://localhost:3000`;
  }

  amIAuthorized(data: Isignin) {
    console.log('🚀 ~ AuthService ~ amIAuthorized ~ data:', data);
    return this.http.post<Response>(`${this.url}/signin`, data, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    });
  }

  verifyOtp(data: any) {
    return this.http.post(`${this.url}/verify`, data);
  }

  signUp(data: Isingup) {
    return this.http.post(`${this.url}/signup`, data);
  }
}
