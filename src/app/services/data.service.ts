// Ng
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';

// Custom Class
import { NotFoundError } from './../common/not-found-error';
import { AppError } from './../common/app-error';

// Rxjs
import { throwError, pipe } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class DataService {
  // private req: IDBOpenDBRequest;
  // private db: IDBDatabase;
  private token: string = localStorage.getItem('token') || '';
  private headers = new HttpHeaders({
    authorization: `Bearer ${this.token}`,
  });

  constructor(@Inject(String) private url: string, private http: HttpClient) {
    // IndexDB
    // this.req = indexedDB.open('QQ', 1);
  }

  getAll() {
    // Prepare the headers with authorization token
    const headers = new HttpHeaders({
      authorization: `Bearer ${this.token}`,
    });

    return this.http.get(this.url, { headers });
    // .pipe(catchError(this.handleError));
  }

  getOne(id: number | string) {
    return this.http.get(this.url + '/' + id, { headers: this.headers });
    // .pipe(catchError(this.handleError));
  }

  update(id: number | string, data: any) {
    return this.http.patch(this.url + '/' + id, data, {
      headers: this.headers,
    });
    // .pipe(catchError(this.handleError));
  }

  // TODO: Implement IndexDB

  // initializeDB() {
  //   this.req.onupgradeneeded = (ev:IDBVersionChangeEvent) => {
  //     const db = ev.target!.result;
  //     const store = db.createObjectStore('QQ', { keyPath: 'id' });
  //     store.createIndex('id', 'id', { unique: true });
  //     store.createIndex('title', 'title', { unique: false });
  //     store.createIndex('body', 'body', { unique: false });
  //     store.createIndex('userId', 'userId', { unique: false });
  //   }

  // }

  // add(data: any) {
  //   this.req.onsuccess = (this: IDBRequest<IDBDatabase>, ev: Event)=>  {
  //     this.db = ev.target.result;
  //     const tx = db.transaction('QQ', 'readwrite');
  //     const store = tx.objectStore('QQ');
  //     const index = store.index('id');
  //     const request = index.getAll();

  //     request.onsuccess = function (event: { target: { result: any; }; }) {
  //       console.log(event.target.result);
  //     }
  //   }
  // }

  //   error () => {

  //     req.onerror = function (event: { target: { error: any; }; }) {
  //       console.log(event.target.error);
  //     }
  //   }
}
